package swagger

//go:generate rm -rf ../../internal/generated
//go:generate mkdir -p ../../internal/generated
//go:generate swagger generate server --quiet --target ../../internal/generated --name api --spec server.yaml --exclude-main
//go:generate swagger generate client --quiet --target ../../internal/generated --name client --spec client/stocks-users.yaml
//go:generate swagger generate client --quiet --target ../../internal/generated --name client --spec client/stonks-news.yaml
