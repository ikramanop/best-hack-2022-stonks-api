package internal

type TargetAction string

const (
	GetUserInfo          TargetAction = "GetUserInfo"
	UpdateUserInfo       TargetAction = "UpdateUserInfo"
	AddPaymentInfo       TargetAction = "AddPaymentInfo"
	DeletePaymentInfo    TargetAction = "DeletePaymentInfo"
	ChangeBalance        TargetAction = "ChangeBalance"
	GetUserTransactions  TargetAction = "GetUserTransactions"
	GetUserPortfolio     TargetAction = "GetUserPortfolio"
	CreateTransaction    TargetAction = "CreateTransaction"
	SetNotification      TargetAction = "SetNotification"
	GetNotifications     TargetAction = "GetNotifications"
	CreateSupportRequest TargetAction = "CreateSupportRequest"
	GetUsersWithParams   TargetAction = "GetUsersWithParams"
	SwitchUserState      TargetAction = "SwitchUserState"
	ChangeUserRole       TargetAction = "ChangeUserRole"
)

type UserRole string

const (
	UserRoleSimple   UserRole = "simple"
	UserRoleVerified UserRole = "verified"
	UserRoleAdmin    UserRole = "admin"
)

var roleMap = map[TargetAction][]UserRole{
	GetUserInfo:          {UserRoleSimple, UserRoleVerified, UserRoleAdmin},
	UpdateUserInfo:       {UserRoleSimple, UserRoleVerified, UserRoleAdmin},
	AddPaymentInfo:       {UserRoleSimple, UserRoleVerified, UserRoleAdmin},
	DeletePaymentInfo:    {UserRoleVerified, UserRoleAdmin},
	ChangeBalance:        {UserRoleVerified, UserRoleAdmin},
	GetUserTransactions:  {UserRoleVerified, UserRoleAdmin},
	GetUserPortfolio:     {UserRoleVerified, UserRoleAdmin},
	CreateTransaction:    {UserRoleVerified, UserRoleAdmin},
	SetNotification:      {UserRoleVerified, UserRoleAdmin},
	GetNotifications:     {UserRoleVerified, UserRoleAdmin},
	CreateSupportRequest: {UserRoleSimple, UserRoleVerified, UserRoleAdmin},
	GetUsersWithParams:   {UserRoleAdmin},
	SwitchUserState:      {UserRoleAdmin},
	ChangeUserRole:       {UserRoleAdmin},
}

func CheckRole(targetAction TargetAction, userRole UserRole) bool {
	roles, ok := roleMap[targetAction]
	if !ok {
		return false
	}

	for _, role := range roles {
		if role == userRole {
			return true
		}
	}

	return false
}
