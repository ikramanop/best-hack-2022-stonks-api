package admin

import (
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/admin"
	"log"
	"os"
)

func ChangeUserRole(params admin.ChangeUserRoleParams) middleware.Responder {
	log.Printf("Hit PATCH /admin/users/{uuid}/changeRole from %s\n", params.HTTPRequest.UserAgent())

	action := internal.ChangeUserRole

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)

	clientAuth := auth.New(transport, strfmt.Default)

	responseAuth, err := clientAuth.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return admin.NewChangeUserRoleBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if !internal.CheckRole(action, internal.UserRole(*responseAuth.Payload.Role)) {
		return admin.NewChangeUserRoleBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported action",
				Status:  false,
			},
		)
	}

	clientUsers := users.New(transport, strfmt.Default)

	responseUsers, err := clientUsers.ChangeUserRole(users.NewChangeUserRoleParams().
		WithUUID(params.UUID).WithNewRole(params.NewRole))
	if err != nil {
		return admin.NewChangeUserRoleBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	return admin.NewChangeUserRoleOK().WithPayload(responseUsers.Payload)
}
