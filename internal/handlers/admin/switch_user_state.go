package admin

import (
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/admin"
	"log"
	"os"
)

func SwitchUserState(params admin.SwitchUserStateParams) middleware.Responder {
	log.Printf("Hit PATCH /admin/users/{uuid}/switch from %s\n", params.HTTPRequest.UserAgent())

	action := internal.SwitchUserState

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)

	clientAuth := auth.New(transport, strfmt.Default)

	responseAuth, err := clientAuth.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return admin.NewSwitchUserStateBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if !internal.CheckRole(action, internal.UserRole(*responseAuth.Payload.Role)) {
		return admin.NewSwitchUserStateBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported action",
				Status:  false,
			},
		)
	}

	clientUsers := users.New(transport, strfmt.Default)

	responseUsers, err := clientUsers.SwitchUserState(users.NewSwitchUserStateParams().WithUUID(params.UUID))
	if err != nil {
		return admin.NewSwitchUserStateBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	return admin.NewSwitchUserStateOK().WithPayload(responseUsers.Payload)
}
