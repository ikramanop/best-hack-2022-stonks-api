package stocks

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/stocks"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/prices"
)

func GetSingleStockHandler(params stocks.GetSingleStockParams) middleware.Responder {
	log.Printf("Hit GET /stocks/{targetCurrency}/{sourceCurrency} from %s\n", params.HTTPRequest.UserAgent())

	rxq, err := prices.New()
	if err != nil {
		return stocks.NewGetSingleStockBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	price, ok, err := rxq.Get(params.TargetCurrency, params.SourceCurrency)
	if err != nil {
		return stocks.NewGetSingleStockBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return stocks.NewGetSingleStockBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "stock not found",
				Status:  false,
			},
		)
	}

	priceSetups, ok, err := rxq.GetPriceSetup(params.TargetCurrency, params.SourceCurrency)
	if err != nil {
		return stocks.NewGetSingleStockBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return stocks.NewGetSingleStockBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "price setup not found",
				Status:  false,
			},
		)
	}

	return stocks.NewGetSingleStockOK().WithPayload(
		&models.Stock{
			Change: models.StockChange{
				Plain:   0,
				Percent: 0,
			},
			Price:          price.Price,
			BuyPrice:       price.Price + (price.Price * priceSetups.BuyDelta),
			SellPrice:      price.Price + (price.Price * priceSetups.SellDelta),
			SourceCurrency: price.SourceCurrency,
			TargetCurrency: price.TargetCurrency,
		},
	)
}
