package stocks

import (
	"log"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/stocks"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/prices"
)

func GetCurrentStocksHandler(params stocks.GetCurrentStocksParams) middleware.Responder {
	log.Printf("Hit GET /stocks/{currency}/current from %s\n", params.HTTPRequest.UserAgent())

	rx, err := prices.New()
	if err != nil {
		return stocks.NewGetCurrentStocksBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	pricesRaw, ok, err := rx.GetCurrent(params.Currency)
	if err != nil {
		return stocks.NewGetCurrentStocksBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return stocks.NewGetCurrentStocksBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "stocks not found",
				Status:  false,
			},
		)
	}

	var stocksResponse []*models.Stock
	for _, price := range pricesRaw {
		stocksResponse = append(stocksResponse, &models.Stock{
			Change: models.StockChange{
				Plain:   0,
				Percent: 0,
			},
			Price:          price.Price,
			SourceCurrency: price.SourceCurrency,
			TargetCurrency: price.TargetCurrency,
		})
	}

	return stocks.NewGetCurrentStocksOK().WithPayload(
		&models.Stocks{Stocks: stocksResponse},
	)
}
