package stocks

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/stocks"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/currencies"
	"log"
)

func GetCurrenciesHandler(params stocks.GetCurrenciesParams) middleware.Responder {
	log.Printf("Hit GET /stocks/currencies from %s\n", params.HTTPRequest.UserAgent())

	rx, err := currencies.New()
	if err != nil {
		return stocks.NewGetCurrenciesBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	currenciesRaw, ok, err := rx.Get()
	if err != nil {
		return stocks.NewGetCurrenciesBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return stocks.NewGetCurrenciesBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "no currencies found",
				Status:  false,
			},
		)
	}

	var currenciesResponse []*models.Currency
	for _, currency := range currenciesRaw {
		currenciesResponse = append(currenciesResponse, &models.Currency{
			Tag:  currency.Tag,
			Name: currency.Name,
		})
	}

	return stocks.NewGetCurrenciesOK().WithPayload(&models.Currencies{Currencies: currenciesResponse})
}
