package stocks

import (
	"log"
	"time"

	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/stocks"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/prices"
)

func GetStocksForGraphHandler(params stocks.GetStocksForGhaphParams) middleware.Responder {
	log.Printf("Hit POST /stocks/{targetCurrency}/{sourceCurrency}/graph from %s\n", params.HTTPRequest.UserAgent())

	var period prices.Period
	switch params.Body.Period {
	case 10:
		period = prices.Period10Minutes
	case 30:
		period = prices.Period30Minutes
	case 60:
		period = prices.Period1Hour
	case 240:
		period = prices.Period4Hours
	default:
		return stocks.NewGetStocksForGhaphBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported period",
				Status:  false,
			},
		)
	}

	rx, err := prices.New()
	if err != nil {
		return stocks.NewGetStocksForGhaphBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	pricesRaw, ok, err := rx.GetWithParams(params.TargetCurrency, params.SourceCurrency, period, int(params.Body.PointsCount))
	if err != nil {
		return stocks.NewGetStocksForGhaphBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return stocks.NewGetStocksForGhaphBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "stocks not found",
				Status:  false,
			},
		)
	}

	var stocksResponse []*models.GraphPoint
	for _, quote := range pricesRaw {
		stocksResponse = append(stocksResponse, &models.GraphPoint{
			Timestamp: quote.Timestamp.Format(time.RFC3339),
			Value:     quote.Price,
		})
	}

	return stocks.NewGetStocksForGhaphOK().WithPayload(
		&models.GraphResponse{Points: stocksResponse},
	)
}
