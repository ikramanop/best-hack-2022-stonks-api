package auth

import (
	"log"
	"os"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"

	auth2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/auth"
)

func LoginUserHandler(params auth.LoginUserParams) middleware.Responder {
	log.Printf("Hit POST /user/login from %s\n", params.HTTPRequest.UserAgent())

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)
	client := auth2.New(transport, strfmt.Default)

	response, err := client.LoginUser(auth2.NewLoginUserParams().
		WithBody(&models.LoginRequest{
			Login:    params.Body.Login,
			Password: params.Body.Password,
		}))
	if err != nil {
		return auth.NewLoginUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	return auth.NewLoginUserOK().WithPayload(response.Payload)
}
