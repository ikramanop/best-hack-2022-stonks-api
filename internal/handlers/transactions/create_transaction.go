package transactions

import (
	"context"
	"errors"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal"
	"log"
	"os"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"github.com/jackc/pgx/v4"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/transactions"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/prices"
	transactions2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/transactions"
)

func CreateTransactionHandler(params transactions.CreateTransactionParams) middleware.Responder {
	log.Printf("Hit POST /transactions/create from %s\n", params.HTTPRequest.UserAgent())

	action := internal.CreateTransaction

	if params.Body.Gain == 0 {
		return transactions.NewGetUserPortfolioBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "pointless transaction",
				Status:  false,
			},
		)
	}

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)

	clientAuth := auth.New(transport, strfmt.Default)

	responseAuth, err := clientAuth.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return transactions.NewGetUserPortfolioBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if !internal.CheckRole(action, internal.UserRole(*responseAuth.Payload.Role)) {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported action",
				Status:  false,
			},
		)
	}

	rxq, err := prices.New()
	if err != nil {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	price, ok, err := rxq.Get("RUB", params.Body.Name)
	if err != nil {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "price not found",
				Status:  false,
			},
		)
	}

	rxt, err := transactions2.New()
	if err != nil {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if params.Body.Gain < 0 {
		portfolio, ok, err := rxt.GetPortfolio(*responseAuth.Payload.UserUUID, "RUB")
		if err != nil {
			return transactions.NewCreateTransactionBadRequest().WithPayload(
				&models.BaseAPIResponse{
					Code:    -1000,
					Message: err.Error(),
					Status:  false,
				},
			)
		}
		if !ok {
			return transactions.NewCreateTransactionBadRequest().WithPayload(
				&models.BaseAPIResponse{
					Code:    -1000,
					Message: "cant get current user stocks",
					Status:  false,
				},
			)
		}

		pass := false
		for _, p := range portfolio {
			if p.Name == params.Body.Name {
				if p.Amount < -params.Body.Gain {
					return transactions.NewCreateTransactionBadRequest().WithPayload(
						&models.BaseAPIResponse{
							Code:    -1000,
							Message: "insufficient amount of currency",
							Status:  false,
						},
					)
				}
				pass = true
			}
		}
		if !pass {
			return transactions.NewCreateTransactionBadRequest().WithPayload(
				&models.BaseAPIResponse{
					Code:    -1000,
					Message: "user has no such currency",
					Status:  false,
				},
			)
		}
	}

	clientUsers := users.New(transport, strfmt.Default)

	responseClient, err := clientUsers.GetUser(users.NewGetUserParams().
		WithUUID(*responseAuth.Payload.UserUUID))
	if err != nil {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if responseClient.Payload.Balance < -float64(params.Body.Gain)*price.Price {
		err = errors.New("insufficient funds")
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	tx, err := rxt.BeginTx(context.Background(), pgx.TxOptions{})
	if err != nil {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	defer func() {
		if err != nil {
			tx.Rollback(context.TODO())
		} else {
			tx.Commit(context.TODO())
		}
	}()

	_, err = clientUsers.ChangeBalance(users.NewChangeBalanceParams().
		WithUUID(*responseAuth.Payload.UserUUID).
		WithBody(&models.ChangeBalanceRequest{
			BalanceChange: float64(params.Body.Gain) * price.Price,
			PaymentInfo:   params.Body.PaymentInfo,
		}))
	if err != nil {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	txt := transactions2.NewTx(tx)
	ok, err = txt.Add(*responseAuth.Payload.UserUUID, params.Body.Name, params.Body.Gain, price.Price)
	if err != nil {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return transactions.NewCreateTransactionBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "cant create transaction",
				Status:  false,
			},
		)
	}

	return transactions.NewCreateTransactionOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}
