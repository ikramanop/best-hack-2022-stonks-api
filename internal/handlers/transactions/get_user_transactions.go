package transactions

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal"
	"log"
	"os"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/transactions"
	transactions2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/transactions"
)

func GetUserTransactionsHandler(params transactions.GetUserTransactionsParams) middleware.Responder {
	log.Printf("Hit GET /transactions from %s\n", params.HTTPRequest.UserAgent())

	action := internal.GetUserTransactions

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)

	clientAuth := auth.New(transport, strfmt.Default)

	responseAuth, err := clientAuth.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return transactions.NewGetUserTransactionsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if !internal.CheckRole(action, internal.UserRole(*responseAuth.Payload.Role)) {
		return transactions.NewGetUserTransactionsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported action",
				Status:  false,
			},
		)
	}

	rx, err := transactions2.New()
	if err != nil {
		return transactions.NewGetUserTransactionsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	transactionsRaw, ok, err := rx.Get(*responseAuth.Payload.UserUUID, params.Limit, params.Offset)
	if err != nil {
		return transactions.NewGetUserTransactionsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return transactions.NewGetUserTransactionsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "transactions not found",
				Status:  false,
			},
		)
	}

	var transactionsResponse []*models.Transaction
	for _, transactionRaw := range transactionsRaw {
		transactionsResponse = append(transactionsResponse, &models.Transaction{
			CreatedAt: transactionRaw.CreatedAt.String(),
			Gain:      transactionRaw.Gain,
			Name:      transactionRaw.Name,
			Price:     transactionRaw.Price,
		})
	}

	return transactions.NewGetUserTransactionsOK().WithPayload(
		&models.TransactionsResponse{Transactions: transactionsResponse},
	)
}
