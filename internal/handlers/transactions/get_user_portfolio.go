package transactions

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal"
	"log"
	"os"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/transactions"
	transactions2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/transactions"
)

func GetUserPortfolioHandler(params transactions.GetUserPortfolioParams) middleware.Responder {
	log.Printf("Hit GET /transactions/{currency}/portfolio from %s\n", params.HTTPRequest.UserAgent())

	action := internal.GetUserPortfolio

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)

	clientAuth := auth.New(transport, strfmt.Default)

	responseAuth, err := clientAuth.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return transactions.NewGetUserPortfolioBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if !internal.CheckRole(action, internal.UserRole(*responseAuth.Payload.Role)) {
		return transactions.NewGetUserPortfolioBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported action",
				Status:  false,
			},
		)
	}

	rx, err := transactions2.New()
	if err != nil {
		return transactions.NewGetUserPortfolioBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	portfolios, ok, err := rx.GetPortfolio(*responseAuth.Payload.UserUUID, params.Currency)
	if err != nil {
		return transactions.NewGetUserPortfolioBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return transactions.NewGetUserPortfolioBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "portfolio not found",
				Status:  false,
			},
		)
	}

	var portfolioResponse []*models.PortfolioItem
	for _, portfolio := range portfolios {
		portfolioResponse = append(portfolioResponse, &models.PortfolioItem{
			Amount: portfolio.Amount,
			Change: &models.PortfolioItemChange{
				Percent: portfolio.ChangePercent,
				Plain:   portfolio.ChangePlain,
			},
			Name:       portfolio.Name,
			TotalPrice: portfolio.TotalPrice,
		})
	}

	return transactions.NewGetUserPortfolioOK().WithPayload(
		&models.PortfolioResponse{Portfolio: portfolioResponse},
	)
}
