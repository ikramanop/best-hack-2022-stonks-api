package support

import (
	"errors"
	"fmt"
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/support"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
)

func CreateSupportRequestHandler(params support.CreateSupportRequestParams) middleware.Responder {
	log.Printf("Hit POST /support/request from %s\n", params.HTTPRequest.UserAgent())

	action := internal.CreateSupportRequest

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)

	clientAuth := auth.New(transport, strfmt.Default)

	responseAuth, err := clientAuth.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return support.NewCreateSupportRequestBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if !internal.CheckRole(action, internal.UserRole(*responseAuth.Payload.Role)) {
		return support.NewCreateSupportRequestBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported action",
				Status:  false,
			},
		)
	}

	clientUsers := users.New(transport, strfmt.Default)

	responseUser, err := clientUsers.GetUser(users.NewGetUserParams().WithUUID(*responseAuth.Payload.UserUUID))
	if err != nil {
		return support.NewCreateSupportRequestBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	payload := fmt.Sprintf(
		`
Пользователь: %s %s
Роль: %s

Тема: %s

Сообщение: %s

Контакты: %s
Предпочитаемый вид связи: %s
`,
		responseUser.Payload.Surname, responseUser.Payload.Name,
		responseUser.Payload.Role,
		params.Body.Title,
		params.Body.Message,
		params.Body.ContactInfo,
		strings.Join(params.Body.PreferredMethods, ", "),
	)

	if err := sendNotificationToChat(url.QueryEscape(payload)); err != nil {
		return support.NewCreateSupportRequestBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	return support.NewCreateSupportRequestOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: "",
			Status:  true,
		},
	)
}

func sendNotificationToChat(payload string) error {
	apiKey := os.Getenv("TG_BOT_API_KEY")
	chatId := os.Getenv("TG_SUPPORT_CHAT_ID")

	urll := fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s", apiKey, chatId, payload)

	resp, err := http.Get(urll)
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return errors.New("tg request status not OK")
	}

	return nil
}
