package notifications

import (
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/notifications"
	notifications2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/notifications"
	"log"
	"os"
)

func GetNotificationsHandler(params notifications.GetNotificationsParams) middleware.Responder {
	log.Printf("Hit GET /notifications/set from %s\n", params.HTTPRequest.UserAgent())

	action := internal.GetNotifications

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)

	clientAuth := auth.New(transport, strfmt.Default)

	responseAuth, err := clientAuth.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return notifications.NewGetNotificationsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if !internal.CheckRole(action, internal.UserRole(*responseAuth.Payload.Role)) {
		return notifications.NewGetNotificationsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported action",
				Status:  false,
			},
		)
	}

	rx, err := notifications2.New()
	if err != nil {
		return notifications.NewGetNotificationsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	notificationsRaw, ok, err := rx.GetNotifications(*responseAuth.Payload.UserUUID)
	if err != nil {
		return notifications.NewGetNotificationsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}
	if !ok {
		return notifications.NewGetNotificationsBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "no notifications found",
				Status:  false,
			},
		)
	}

	var notificationsResponse []*models.Notification
	for _, notificationRaw := range notificationsRaw {
		notificationsResponse = append(notificationsResponse, &models.Notification{
			Payload: notificationRaw.Payload,
			Seen:    notificationRaw.Seen,
		})
	}

	return notifications.NewGetNotificationsOK().WithPayload(&models.Notifications{Notifications: notificationsResponse})
}
