package users

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal"
	"log"
	"os"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/users"
)

func DeletePaymentInfoHandler(params users.DeletePaymentInfoParams) middleware.Responder {
	log.Printf("Hit DELETE /user/paymentInfo from %s\n", params.HTTPRequest.UserAgent())

	action := internal.DeletePaymentInfo

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)

	clientAuth := auth.New(transport, strfmt.Default)

	responseAuth, err := clientAuth.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return users.NewDeletePaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if !internal.CheckRole(action, internal.UserRole(*responseAuth.Payload.Role)) {
		return users.NewDeletePaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported action",
				Status:  false,
			},
		)
	}

	clientUsers := users2.New(transport, strfmt.Default)

	response, err := clientUsers.DeletePaymentInfo(users2.NewDeletePaymentInfoParams().
		WithUUID(*responseAuth.Payload.UserUUID).
		WithBody(users2.DeletePaymentInfoBody{CardNumber: params.Body.CardNumber}))
	if err != nil {
		return users.NewDeletePaymentInfoBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	return users.NewDeletePaymentInfoOK().WithPayload(response.Payload)
}
