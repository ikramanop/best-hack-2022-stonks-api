package users

import (
	"log"
	"os"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"

	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/users"
)

func RegisterUserHandler(params users.RegisterUserParams) middleware.Responder {
	log.Printf("Hit POST /user/register from %s\n", params.HTTPRequest.UserAgent())

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)
	client := users2.New(transport, strfmt.Default)

	response, err := client.RegisterUser(users2.NewRegisterUserParams().
		WithBody(&models.RegisterRequest{
			Email:      params.Body.Email,
			Name:       params.Body.Name,
			Login:      params.Body.Login,
			Password:   params.Body.Password,
			Patronymic: params.Body.Patronymic,
			Surname:    params.Body.Surname,
		}))
	if err != nil {
		return users.NewRegisterUserBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	return users.NewRegisterUserOK().WithPayload(response.Payload)
}
