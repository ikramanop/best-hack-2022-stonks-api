package users

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal"
	"log"
	"os"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/users"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/users"
)

func ChangeBalanceHandler(params users.ChangeBalanceParams) middleware.Responder {
	log.Printf("Hit PATCH /user/balance from %s\n", params.HTTPRequest.UserAgent())

	action := internal.ChangeBalance

	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)

	clientAuth := auth.New(transport, strfmt.Default)

	responseAuth, err := clientAuth.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	if !internal.CheckRole(action, internal.UserRole(*responseAuth.Payload.Role)) {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: "unsupported action",
				Status:  false,
			},
		)
	}

	clientUsers := users2.New(transport, strfmt.Default)

	response, err := clientUsers.ChangeBalance(users2.NewChangeBalanceParams().
		WithUUID(*responseAuth.Payload.UserUUID).
		WithBody(params.Body))
	if err != nil {
		return users.NewChangeBalanceBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	return users.NewChangeBalanceOK().WithPayload(response.Payload)
}
