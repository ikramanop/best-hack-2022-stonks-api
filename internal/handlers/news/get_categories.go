package news

import (
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	news2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/news"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/news"
	"log"
	"os"
)

func GetCategoriesHandler(params news.GetCategoriesParams) middleware.Responder {
	log.Printf("Hit GET /news/categories from %s\n", params.HTTPRequest.UserAgent())

	transport := httptransport.New(os.Getenv("STONKS_NEWS_HOST"), "/v1", nil)

	client := news2.New(transport, strfmt.Default)
	response, err := client.GetCategories(news2.NewGetCategoriesParams())
	if err != nil {
		return news.NewGetCategoriesBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	return news.NewGetCategoriesOK().WithPayload(response.Payload)
}
