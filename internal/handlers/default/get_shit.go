package _default

import (
	"os"

	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/client/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/default_operations"
)

func GetShitHandler(params default_operations.GetShitParams) middleware.Responder {
	transport := httptransport.New(os.Getenv("STONKS_USERS_HOST"), "/v1", nil)
	client := auth.New(transport, strfmt.Default)

	response, err := client.CheckSession(auth.NewCheckSessionParams().
		WithRequest(&models.CheckSessionRequest{Token: params.XSessionID}))
	if err != nil {
		return default_operations.NewGetShitBadRequest().WithPayload(
			&models.BaseAPIResponse{
				Code:    -1000,
				Message: err.Error(),
				Status:  false,
			},
		)
	}

	userUUID := *response.Payload.UserUUID

	return default_operations.NewGetShitOK().WithPayload(
		&models.BaseAPIResponse{
			Code:    0,
			Message: userUUID,
			Status:  true,
		},
	)
}
