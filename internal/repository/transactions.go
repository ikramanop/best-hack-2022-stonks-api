package repository

import "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"

type Transactions interface {
	Get(userUUID string, limit int64, offset int64) ([]model.Transaction, bool, error)
	GetPortfolio(userUUID string) ([]model.Portfolio, bool, error)
}

type TransactionsTx interface {
	Add(userUUID string, name string, gain int64, price float64) (bool, error)
}
