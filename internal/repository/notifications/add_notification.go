package notifications

import "context"

func (r *Notifications) AddNotification(userUUID, payload string) (bool, error) {
	query := `
insert into notifications (user_uuid, payload)
values ($1, $2)
`

	_, err := r.Exec(context.Background(), query, userUUID, payload)
	if err != nil {
		return false, err
	}

	return true, nil
}
