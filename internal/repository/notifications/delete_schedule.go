package notifications

import "context"

func (r *Notifications) DeleteSchedule(id uint64) (bool, error) {
	query := `
delete from schedules
where id = $1
`

	_, err := r.Exec(context.Background(), query, id)
	if err != nil {
		return false, err
	}

	return true, nil
}
