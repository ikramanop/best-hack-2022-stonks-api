package notifications

import (
	"context"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
)

func (r *Notifications) GetSchedules() ([]model.Schedule, bool, error) {
	query := `
select id, user_uuid, percent_change, source_currency, target_currency, created_at
from schedules
`

	rows, err := r.Query(context.Background(), query)
	if err != nil {
		return nil, false, err
	}
	defer rows.Close()

	var schedules []model.Schedule
	for rows.Next() {
		var schedule model.Schedule
		err := rows.Scan(&schedule.ID, &schedule.UserUUID, &schedule.PercentChange, &schedule.SourceCurrency,
			&schedule.TargetCurrency, &schedule.CreatedAt)
		if err != nil {
			return nil, false, err
		}
		schedules = append(schedules, schedule)
	}

	return schedules, true, nil
}
