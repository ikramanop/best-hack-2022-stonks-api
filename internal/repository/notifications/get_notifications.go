package notifications

import (
	"context"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
)

func (r *Notifications) GetNotifications(userUUID string) ([]model.Notification, bool, error) {
	query := `
select id, user_uuid, payload, seen, created_at
from notifications
where user_uuid = $1
`

	rows, err := r.Query(context.Background(), query, userUUID)
	if err != nil {
		return nil, false, err
	}
	defer rows.Close()

	var notifications []model.Notification
	for rows.Next() {
		var notification model.Notification
		err := rows.Scan(&notification.ID, &notification.UserUUID, &notification.Payload,
			&notification.Seen, &notification.CreatedAt)
		if err != nil {
			return nil, false, err
		}
		notifications = append(notifications, notification)
	}

	return notifications, true, nil
}
