package notifications

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4"
)

type Notifications struct {
	*pgx.Conn
}

func New() (Notifications, error) {
	db, err := pgx.Connect(context.Background(), os.Getenv("STONKS_API_DSN"))
	if err != nil {
		return Notifications{}, err
	}

	return Notifications{db}, nil
}
