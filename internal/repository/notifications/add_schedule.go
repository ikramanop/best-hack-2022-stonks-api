package notifications

import "context"

func (r *Notifications) AddSchedule(userUUID string, percentChange float64, targetCurrency, sourceCurrency string) (bool, error) {
	query := `
insert into schedules (user_uuid, percent_change, source_currency, target_currency)
values ($1, $2, $3, $4)
`

	_, err := r.Exec(context.Background(), query, userUUID, percentChange, sourceCurrency, targetCurrency)
	if err != nil {
		return false, err
	}

	return true, nil
}
