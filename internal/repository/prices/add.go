package prices

import (
	"context"
)

func (r *Prices) Add(sourceCurrency string, targetCurrency string, price float64) (bool, error) {
	query := `
insert into prices (source_currency, target_currency, price)
values ($1, $2, $3)
`
	_, err := r.Exec(context.Background(), query, sourceCurrency, targetCurrency, price)
	if err != nil {
		return false, err
	}

	return true, nil
}
