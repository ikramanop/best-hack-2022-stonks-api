package prices

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/currencies"
)

func (r *Prices) GetCurrent(targetCurrency string) ([]model.Price, bool, error) {
	rx, err := currencies.New()
	if err != nil {
		return nil, false, err
	}

	currenciesRaw, ok, err := rx.Get()
	if err != nil {
		return nil, false, err
	}
	if !ok {
		return nil, ok, nil
	}

	var prices []model.Price
	for _, currencyRaw := range currenciesRaw {
		if currencyRaw.Tag == targetCurrency {
			continue
		}
		price, ok, err := r.Get(targetCurrency, currencyRaw.Tag)
		if err != nil {
			return nil, false, err
		}
		if !ok {
			return nil, ok, nil
		}
		prices = append(prices, price)
	}

	return prices, true, nil
}
