package prices

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"time"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
)

func (r *Prices) GetWithDate(targetCurrency, sourceCurrency string, date time.Time) (model.Price, bool, error) {
	query := `
select id, source_currency, target_currency,
price, timestamp
from prices
where source_currency = $1
and target_currency = $2
and timestamp <= $3
order by timestamp desc
limit 1
`

	var price model.Price
	err := r.QueryRow(context.Background(), query, sourceCurrency, targetCurrency, date).Scan(
		&price.ID, &price.SourceCurrency, &price.TargetCurrency,
		&price.Price, &price.Timestamp,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return model.Price{}, false, nil
		}
		return model.Price{}, false, err
	}

	return price, true, nil
}
