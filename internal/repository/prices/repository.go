package prices

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4"
)

type Prices struct {
	*pgx.Conn
}

func New() (Prices, error) {
	db, err := pgx.Connect(context.Background(), os.Getenv("STONKS_API_DSN"))
	if err != nil {
		return Prices{}, err
	}

	return Prices{db}, nil
}
