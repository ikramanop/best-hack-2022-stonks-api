package prices

import (
	"context"
	"fmt"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
)

func (r *Prices) GetWithParams(targetCurrency, sourceCurrency string, period Period, pointsCount int) ([]model.Price, bool, error) {
	queryCount := `
select count(1)
from prices
where target_currency = $1
and source_currency = $2
and timestamp > (now() - $3 * interval '1 minute')
`

	var count int
	err := r.QueryRow(context.Background(), queryCount, targetCurrency, sourceCurrency, period).Scan(&count)
	if err != nil {
		return nil, false, err
	}

	var query string
	if pointsCount >= count {
		query = `
select id, source_currency, target_currency,
price, timestamp
from prices
where target_currency = $1
and source_currency = $2
and timestamp > (now() - $3 * interval '1 day')
order by timestamp desc
`
	} else {
		query = `select q.id, q.source_currency, q.target_currency,
q.price, q.timestamp from (select id, source_currency, target_currency,
price, timestamp,
row_number() over (order by timestamp) row
      from prices
      where target_currency = $1
		and source_currency = $2
        and timestamp > (now() - $3 * interval '1 day')
      order by timestamp desc) q
where q.row % 
` + fmt.Sprintf("%d = 0", count/pointsCount)
	}

	var prices []model.Price
	rows, err := r.Query(context.Background(), query, targetCurrency, sourceCurrency, period)
	if err != nil {
		return nil, false, err
	}
	defer rows.Close()

	for rows.Next() {
		var price model.Price
		err := rows.
			Scan(
				&price.ID, &price.SourceCurrency, &price.TargetCurrency,
				&price.Price, &price.Timestamp,
			)
		if err != nil {
			return nil, false, err
		}
		prices = append(prices, price)
	}

	return prices, true, nil
}

type Period int

const (
	Period10Minutes Period = 10
	Period30Minutes Period = 30
	Period1Hour     Period = 60
	Period4Hours    Period = 240
)
