package prices

import (
	"context"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
)

func (r *Prices) GetPriceSetup(targetCurrency, sourceCurrency string) (model.PriceSetup, bool, error) {
	query := `
select id, target_currency, source_currency, buy_delta, sell_delta
from price_setups
where target_currency = $1 and source_currency = $2
limit 1
`
	var setup model.PriceSetup
	err := r.QueryRow(context.Background(), query, targetCurrency, sourceCurrency).Scan(
		&setup.ID, &setup.TargetCurrency, &setup.SourceCurrency, &setup.BuyDelta, &setup.SellDelta,
	)
	if err != nil {
		return model.PriceSetup{}, false, err
	}

	return setup, true, nil
}
