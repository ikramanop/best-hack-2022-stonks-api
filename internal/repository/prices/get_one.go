package prices

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
)

func (r *Prices) Get(targetCurrency, sourceCurrency string) (model.Price, bool, error) {
	query := `
select id, source_currency, target_currency,
price, timestamp
from prices
where source_currency = $1
and target_currency = $2
order by timestamp desc
limit 1
`

	var price model.Price
	err := r.QueryRow(context.Background(), query, sourceCurrency, targetCurrency).Scan(
		&price.ID, &price.SourceCurrency, &price.TargetCurrency,
		&price.Price, &price.Timestamp,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return model.Price{}, false, nil
		}
		return model.Price{}, false, err
	}

	return price, true, nil
}
