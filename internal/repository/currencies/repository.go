package currencies

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4"
)

type Currencies struct {
	*pgx.Conn
}

func New() (Currencies, error) {
	db, err := pgx.Connect(context.Background(), os.Getenv("STONKS_API_DSN"))
	if err != nil {
		return Currencies{}, err
	}

	return Currencies{db}, nil
}
