package currencies

import (
	"context"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
)

func (r *Currencies) Get() ([]model.Currency, bool, error) {
	query := `
select id, tag, name
from currencies
`

	var currencies []model.Currency
	rows, err := r.Query(context.Background(), query)
	if err != nil {
		return nil, false, err
	}
	defer rows.Close()

	for rows.Next() {
		var currency model.Currency
		err := rows.
			Scan(&currency.ID, &currency.Tag, &currency.Name)
		if err != nil {
			return nil, false, err
		}
		currencies = append(currencies, currency)
	}

	return currencies, true, nil
}
