package currencies

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v4"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
)

func (r *Currencies) GetOne(tag string) (model.Currency, bool, error) {
	query := `
select id, tag, name from currencies
where tag = $1
`

	var currency model.Currency
	err := r.QueryRow(context.Background(), query, tag).Scan(&currency.ID, &currency.Tag, &currency.Name)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return model.Currency{}, false, err
		}
	}

	return currency, true, nil
}
