package stocks

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4"
)

type Transactions struct {
	*pgx.Conn
}

type TransactionsTx struct {
	pgx.Tx
}

func New() (Transactions, error) {
	db, err := pgx.Connect(context.Background(), os.Getenv("STONKS_API_DSN"))
	if err != nil {
		return Transactions{}, err
	}

	return Transactions{db}, nil
}

func NewTx(tx pgx.Tx) TransactionsTx {
	return TransactionsTx{tx}
}
