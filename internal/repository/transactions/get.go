package stocks

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v4"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
)

func (r *Transactions) Get(userUUID string, limit int64, offset int64) ([]model.Transaction, bool, error) {
	query := `
select id, user_uuid, name, gain, price, created_at
from transactions
where user_uuid = $1
limit $2 offset $3
`

	var transactions []model.Transaction
	rows, err := r.Query(context.Background(), query, userUUID, limit, offset)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, true, nil
		}

		return nil, false, err
	}
	defer rows.Close()

	for rows.Next() {
		var transaction model.Transaction
		err := rows.
			Scan(&transaction.ID, &transaction.UserUUID, &transaction.Name,
				&transaction.Gain, &transaction.Price, &transaction.CreatedAt)
		if err != nil {
			return nil, false, err
		}
		transactions = append(transactions, transaction)
	}

	return transactions, true, nil
}
