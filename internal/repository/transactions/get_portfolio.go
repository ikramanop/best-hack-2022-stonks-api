package stocks

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/prices"
)

func (r *Transactions) GetPortfolio(userUUID string, targetCurrency string) ([]model.Portfolio, bool, error) {
	query := `
select t.name, sum(t.total_price), sum(t.gain)
from (
         select name, gain * price total_price, gain
         from transactions
         where user_uuid = $1
     ) t
group by t.name
`

	var portfolios []model.Portfolio
	rows, err := r.Query(context.Background(), query, userUUID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, true, nil
		}

		return nil, false, err
	}
	defer rows.Close()

	for rows.Next() {
		var portfolio model.Portfolio
		err := rows.Scan(&portfolio.Name, &portfolio.TotalPrice, &portfolio.Amount)
		if err != nil {
			return nil, false, err
		}
		portfolios = append(portfolios, portfolio)
	}

	rx, err := prices.New()
	if err != nil {
		return nil, false, err
	}
	pricesRaw, ok, err := rx.GetCurrent(targetCurrency)
	if err != nil {
		return nil, false, err
	}
	if !ok {
		return nil, false, nil
	}

	for i, portfolio := range portfolios {
		for _, price := range pricesRaw {
			if portfolio.Name == price.TargetCurrency {
				current := price.Price * float64(portfolio.Amount)
				portfolios[i].ChangePlain = current - portfolio.TotalPrice
				portfolios[i].ChangePercent = (current - portfolio.TotalPrice) / current
			}
		}
	}

	return portfolios, false, nil
}
