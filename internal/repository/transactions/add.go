package stocks

import (
	"context"
)

func (r *TransactionsTx) Add(userUUID string, name string, gain int64, price float64) (bool, error) {
	query := `
insert into transactions (userUUID, name, gain, price)
values ($1, $2, $3, $4)
`
	_, err := r.Exec(context.Background(), query, userUUID, name, gain, price)
	if err != nil {
		return false, err
	}

	return true, nil
}
