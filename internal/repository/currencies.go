package repository

import "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/model"

type Currencies interface {
	Get() ([]model.Currency, bool, error)
	GetOne(tag string) (model.Currency, bool, error)
}
