// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// CheckSessionRequest check session request
//
// swagger:model CheckSessionRequest
type CheckSessionRequest struct {

	// token
	// Required: true
	Token string `json:"token"`
}

// Validate validates this check session request
func (m *CheckSessionRequest) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateToken(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *CheckSessionRequest) validateToken(formats strfmt.Registry) error {

	if err := validate.RequiredString("token", "body", m.Token); err != nil {
		return err
	}

	return nil
}

// ContextValidate validates this check session request based on context it is used
func (m *CheckSessionRequest) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *CheckSessionRequest) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *CheckSessionRequest) UnmarshalBinary(b []byte) error {
	var res CheckSessionRequest
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
