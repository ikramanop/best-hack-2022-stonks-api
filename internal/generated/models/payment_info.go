// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"encoding/json"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// PaymentInfo payment info
//
// swagger:model PaymentInfo
type PaymentInfo struct {

	// card holder
	// Required: true
	CardHolder string `json:"cardHolder"`

	// card number
	// Required: true
	CardNumber string `json:"cardNumber"`

	// status
	// Required: true
	// Enum: [active expired]
	Status string `json:"status"`

	// till date
	// Required: true
	TillDate *PaymentInfoTillDate `json:"tillDate"`
}

// Validate validates this payment info
func (m *PaymentInfo) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateCardHolder(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateCardNumber(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateStatus(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateTillDate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *PaymentInfo) validateCardHolder(formats strfmt.Registry) error {

	if err := validate.RequiredString("cardHolder", "body", m.CardHolder); err != nil {
		return err
	}

	return nil
}

func (m *PaymentInfo) validateCardNumber(formats strfmt.Registry) error {

	if err := validate.RequiredString("cardNumber", "body", m.CardNumber); err != nil {
		return err
	}

	return nil
}

var paymentInfoTypeStatusPropEnum []interface{}

func init() {
	var res []string
	if err := json.Unmarshal([]byte(`["active","expired"]`), &res); err != nil {
		panic(err)
	}
	for _, v := range res {
		paymentInfoTypeStatusPropEnum = append(paymentInfoTypeStatusPropEnum, v)
	}
}

const (

	// PaymentInfoStatusActive captures enum value "active"
	PaymentInfoStatusActive string = "active"

	// PaymentInfoStatusExpired captures enum value "expired"
	PaymentInfoStatusExpired string = "expired"
)

// prop value enum
func (m *PaymentInfo) validateStatusEnum(path, location string, value string) error {
	if err := validate.EnumCase(path, location, value, paymentInfoTypeStatusPropEnum, true); err != nil {
		return err
	}
	return nil
}

func (m *PaymentInfo) validateStatus(formats strfmt.Registry) error {

	if err := validate.RequiredString("status", "body", m.Status); err != nil {
		return err
	}

	// value enum
	if err := m.validateStatusEnum("status", "body", m.Status); err != nil {
		return err
	}

	return nil
}

func (m *PaymentInfo) validateTillDate(formats strfmt.Registry) error {

	if err := validate.Required("tillDate", "body", m.TillDate); err != nil {
		return err
	}

	if m.TillDate != nil {
		if err := m.TillDate.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("tillDate")
			} else if ce, ok := err.(*errors.CompositeError); ok {
				return ce.ValidateName("tillDate")
			}
			return err
		}
	}

	return nil
}

// ContextValidate validate this payment info based on the context it is used
func (m *PaymentInfo) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := m.contextValidateTillDate(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *PaymentInfo) contextValidateTillDate(ctx context.Context, formats strfmt.Registry) error {

	if m.TillDate != nil {
		if err := m.TillDate.ContextValidate(ctx, formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("tillDate")
			} else if ce, ok := err.(*errors.CompositeError); ok {
				return ce.ValidateName("tillDate")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *PaymentInfo) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PaymentInfo) UnmarshalBinary(b []byte) error {
	var res PaymentInfo
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// PaymentInfoTillDate payment info till date
//
// swagger:model PaymentInfoTillDate
type PaymentInfoTillDate struct {

	// month
	// Required: true
	Month int64 `json:"month"`

	// year
	// Required: true
	Year int64 `json:"year"`
}

// Validate validates this payment info till date
func (m *PaymentInfoTillDate) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateMonth(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateYear(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *PaymentInfoTillDate) validateMonth(formats strfmt.Registry) error {

	if err := validate.Required("tillDate"+"."+"month", "body", int64(m.Month)); err != nil {
		return err
	}

	return nil
}

func (m *PaymentInfoTillDate) validateYear(formats strfmt.Registry) error {

	if err := validate.Required("tillDate"+"."+"year", "body", int64(m.Year)); err != nil {
		return err
	}

	return nil
}

// ContextValidate validates this payment info till date based on context it is used
func (m *PaymentInfoTillDate) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *PaymentInfoTillDate) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PaymentInfoTillDate) UnmarshalBinary(b []byte) error {
	var res PaymentInfoTillDate
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
