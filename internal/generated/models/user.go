// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"strconv"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// User user
//
// swagger:model User
type User struct {

	// balance
	// Required: true
	Balance float64 `json:"balance"`

	// email
	// Required: true
	Email string `json:"email"`

	// login
	// Required: true
	Login string `json:"login"`

	// name
	// Required: true
	Name string `json:"name"`

	// patronymic
	// Required: true
	Patronymic *string `json:"patronymic"`

	// payment methods
	PaymentMethods []*PaymentInfo `json:"paymentMethods"`

	// role
	// Required: true
	Role string `json:"role"`

	// surname
	// Required: true
	Surname string `json:"surname"`
}

// Validate validates this user
func (m *User) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateBalance(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateEmail(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateLogin(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateName(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validatePatronymic(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validatePaymentMethods(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateRole(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateSurname(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *User) validateBalance(formats strfmt.Registry) error {

	if err := validate.Required("balance", "body", float64(m.Balance)); err != nil {
		return err
	}

	return nil
}

func (m *User) validateEmail(formats strfmt.Registry) error {

	if err := validate.RequiredString("email", "body", m.Email); err != nil {
		return err
	}

	return nil
}

func (m *User) validateLogin(formats strfmt.Registry) error {

	if err := validate.RequiredString("login", "body", m.Login); err != nil {
		return err
	}

	return nil
}

func (m *User) validateName(formats strfmt.Registry) error {

	if err := validate.RequiredString("name", "body", m.Name); err != nil {
		return err
	}

	return nil
}

func (m *User) validatePatronymic(formats strfmt.Registry) error {

	if err := validate.Required("patronymic", "body", m.Patronymic); err != nil {
		return err
	}

	return nil
}

func (m *User) validatePaymentMethods(formats strfmt.Registry) error {
	if swag.IsZero(m.PaymentMethods) { // not required
		return nil
	}

	for i := 0; i < len(m.PaymentMethods); i++ {
		if swag.IsZero(m.PaymentMethods[i]) { // not required
			continue
		}

		if m.PaymentMethods[i] != nil {
			if err := m.PaymentMethods[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("paymentMethods" + "." + strconv.Itoa(i))
				} else if ce, ok := err.(*errors.CompositeError); ok {
					return ce.ValidateName("paymentMethods" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

func (m *User) validateRole(formats strfmt.Registry) error {

	if err := validate.RequiredString("role", "body", m.Role); err != nil {
		return err
	}

	return nil
}

func (m *User) validateSurname(formats strfmt.Registry) error {

	if err := validate.RequiredString("surname", "body", m.Surname); err != nil {
		return err
	}

	return nil
}

// ContextValidate validate this user based on the context it is used
func (m *User) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	var res []error

	if err := m.contextValidatePaymentMethods(ctx, formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *User) contextValidatePaymentMethods(ctx context.Context, formats strfmt.Registry) error {

	for i := 0; i < len(m.PaymentMethods); i++ {

		if m.PaymentMethods[i] != nil {
			if err := m.PaymentMethods[i].ContextValidate(ctx, formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("paymentMethods" + "." + strconv.Itoa(i))
				} else if ce, ok := err.(*errors.CompositeError); ok {
					return ce.ValidateName("paymentMethods" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (m *User) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *User) UnmarshalBinary(b []byte) error {
	var res User
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
