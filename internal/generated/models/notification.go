// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// Notification notification
//
// swagger:model Notification
type Notification struct {

	// payload
	// Required: true
	Payload string `json:"payload"`

	// seen
	// Required: true
	Seen bool `json:"seen"`
}

// Validate validates this notification
func (m *Notification) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validatePayload(formats); err != nil {
		res = append(res, err)
	}

	if err := m.validateSeen(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *Notification) validatePayload(formats strfmt.Registry) error {

	if err := validate.RequiredString("payload", "body", m.Payload); err != nil {
		return err
	}

	return nil
}

func (m *Notification) validateSeen(formats strfmt.Registry) error {

	if err := validate.Required("seen", "body", bool(m.Seen)); err != nil {
		return err
	}

	return nil
}

// ContextValidate validates this notification based on context it is used
func (m *Notification) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *Notification) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Notification) UnmarshalBinary(b []byte) error {
	var res Notification
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
