// Code generated by go-swagger; DO NOT EDIT.

package notifications

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
)

// SetNotificationOKCode is the HTTP code returned for type SetNotificationOK
const SetNotificationOKCode int = 200

/*SetNotificationOK Success

swagger:response setNotificationOK
*/
type SetNotificationOK struct {

	/*
	  In: Body
	*/
	Payload *models.BaseAPIResponse `json:"body,omitempty"`
}

// NewSetNotificationOK creates SetNotificationOK with default headers values
func NewSetNotificationOK() *SetNotificationOK {

	return &SetNotificationOK{}
}

// WithPayload adds the payload to the set notification o k response
func (o *SetNotificationOK) WithPayload(payload *models.BaseAPIResponse) *SetNotificationOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the set notification o k response
func (o *SetNotificationOK) SetPayload(payload *models.BaseAPIResponse) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *SetNotificationOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// SetNotificationBadRequestCode is the HTTP code returned for type SetNotificationBadRequest
const SetNotificationBadRequestCode int = 400

/*SetNotificationBadRequest Failure

swagger:response setNotificationBadRequest
*/
type SetNotificationBadRequest struct {

	/*
	  In: Body
	*/
	Payload *models.BaseAPIResponse `json:"body,omitempty"`
}

// NewSetNotificationBadRequest creates SetNotificationBadRequest with default headers values
func NewSetNotificationBadRequest() *SetNotificationBadRequest {

	return &SetNotificationBadRequest{}
}

// WithPayload adds the payload to the set notification bad request response
func (o *SetNotificationBadRequest) WithPayload(payload *models.BaseAPIResponse) *SetNotificationBadRequest {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the set notification bad request response
func (o *SetNotificationBadRequest) SetPayload(payload *models.BaseAPIResponse) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *SetNotificationBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(400)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
