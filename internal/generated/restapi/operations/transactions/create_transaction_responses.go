// Code generated by go-swagger; DO NOT EDIT.

package transactions

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
)

// CreateTransactionOKCode is the HTTP code returned for type CreateTransactionOK
const CreateTransactionOKCode int = 200

/*CreateTransactionOK Success

swagger:response createTransactionOK
*/
type CreateTransactionOK struct {

	/*
	  In: Body
	*/
	Payload *models.BaseAPIResponse `json:"body,omitempty"`
}

// NewCreateTransactionOK creates CreateTransactionOK with default headers values
func NewCreateTransactionOK() *CreateTransactionOK {

	return &CreateTransactionOK{}
}

// WithPayload adds the payload to the create transaction o k response
func (o *CreateTransactionOK) WithPayload(payload *models.BaseAPIResponse) *CreateTransactionOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the create transaction o k response
func (o *CreateTransactionOK) SetPayload(payload *models.BaseAPIResponse) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *CreateTransactionOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// CreateTransactionBadRequestCode is the HTTP code returned for type CreateTransactionBadRequest
const CreateTransactionBadRequestCode int = 400

/*CreateTransactionBadRequest Failure

swagger:response createTransactionBadRequest
*/
type CreateTransactionBadRequest struct {

	/*
	  In: Body
	*/
	Payload *models.BaseAPIResponse `json:"body,omitempty"`
}

// NewCreateTransactionBadRequest creates CreateTransactionBadRequest with default headers values
func NewCreateTransactionBadRequest() *CreateTransactionBadRequest {

	return &CreateTransactionBadRequest{}
}

// WithPayload adds the payload to the create transaction bad request response
func (o *CreateTransactionBadRequest) WithPayload(payload *models.BaseAPIResponse) *CreateTransactionBadRequest {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the create transaction bad request response
func (o *CreateTransactionBadRequest) SetPayload(payload *models.BaseAPIResponse) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *CreateTransactionBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(400)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
