// Code generated by go-swagger; DO NOT EDIT.

package stocks

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"io"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/validate"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
)

// NewGetStocksForGhaphParams creates a new GetStocksForGhaphParams object
//
// There are no default values defined in the spec.
func NewGetStocksForGhaphParams() GetStocksForGhaphParams {

	return GetStocksForGhaphParams{}
}

// GetStocksForGhaphParams contains all the bound params for the get stocks for ghaph operation
// typically these are obtained from a http.Request
//
// swagger:parameters GetStocksForGhaph
type GetStocksForGhaphParams struct {

	// HTTP Request Object
	HTTPRequest *http.Request `json:"-"`

	/*
	  Required: true
	  In: body
	*/
	Body *models.GraphRequest
	/*
	  Required: true
	  In: path
	*/
	SourceCurrency string
	/*
	  Required: true
	  In: path
	*/
	TargetCurrency string
}

// BindRequest both binds and validates a request, it assumes that complex things implement a Validatable(strfmt.Registry) error interface
// for simple values it will use straight method calls.
//
// To ensure default values, the struct must have been initialized with NewGetStocksForGhaphParams() beforehand.
func (o *GetStocksForGhaphParams) BindRequest(r *http.Request, route *middleware.MatchedRoute) error {
	var res []error

	o.HTTPRequest = r

	if runtime.HasBody(r) {
		defer r.Body.Close()
		var body models.GraphRequest
		if err := route.Consumer.Consume(r.Body, &body); err != nil {
			if err == io.EOF {
				res = append(res, errors.Required("body", "body", ""))
			} else {
				res = append(res, errors.NewParseError("body", "body", "", err))
			}
		} else {
			// validate body object
			if err := body.Validate(route.Formats); err != nil {
				res = append(res, err)
			}

			ctx := validate.WithOperationRequest(context.Background())
			if err := body.ContextValidate(ctx, route.Formats); err != nil {
				res = append(res, err)
			}

			if len(res) == 0 {
				o.Body = &body
			}
		}
	} else {
		res = append(res, errors.Required("body", "body", ""))
	}

	rSourceCurrency, rhkSourceCurrency, _ := route.Params.GetOK("sourceCurrency")
	if err := o.bindSourceCurrency(rSourceCurrency, rhkSourceCurrency, route.Formats); err != nil {
		res = append(res, err)
	}

	rTargetCurrency, rhkTargetCurrency, _ := route.Params.GetOK("targetCurrency")
	if err := o.bindTargetCurrency(rTargetCurrency, rhkTargetCurrency, route.Formats); err != nil {
		res = append(res, err)
	}
	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// bindSourceCurrency binds and validates parameter SourceCurrency from path.
func (o *GetStocksForGhaphParams) bindSourceCurrency(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: true
	// Parameter is provided by construction from the route
	o.SourceCurrency = raw

	return nil
}

// bindTargetCurrency binds and validates parameter TargetCurrency from path.
func (o *GetStocksForGhaphParams) bindTargetCurrency(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: true
	// Parameter is provided by construction from the route
	o.TargetCurrency = raw

	return nil
}
