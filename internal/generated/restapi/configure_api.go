// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/admin"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/default_operations"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/news"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/notifications"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/stocks"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/support"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/transactions"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/users"
)

//go:generate swagger generate server --target ../../generated --name API --spec ../../../pkg/swagger/server.yaml --principal interface{} --exclude-main

func configureFlags(api *operations.APIAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.APIAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	if api.UsersAddPaymentInfoHandler == nil {
		api.UsersAddPaymentInfoHandler = users.AddPaymentInfoHandlerFunc(func(params users.AddPaymentInfoParams) middleware.Responder {
			return middleware.NotImplemented("operation users.AddPaymentInfo has not yet been implemented")
		})
	}
	if api.UsersChangeBalanceHandler == nil {
		api.UsersChangeBalanceHandler = users.ChangeBalanceHandlerFunc(func(params users.ChangeBalanceParams) middleware.Responder {
			return middleware.NotImplemented("operation users.ChangeBalance has not yet been implemented")
		})
	}
	if api.AdminChangeUserRoleHandler == nil {
		api.AdminChangeUserRoleHandler = admin.ChangeUserRoleHandlerFunc(func(params admin.ChangeUserRoleParams) middleware.Responder {
			return middleware.NotImplemented("operation admin.ChangeUserRole has not yet been implemented")
		})
	}
	if api.SupportCreateSupportRequestHandler == nil {
		api.SupportCreateSupportRequestHandler = support.CreateSupportRequestHandlerFunc(func(params support.CreateSupportRequestParams) middleware.Responder {
			return middleware.NotImplemented("operation support.CreateSupportRequest has not yet been implemented")
		})
	}
	if api.TransactionsCreateTransactionHandler == nil {
		api.TransactionsCreateTransactionHandler = transactions.CreateTransactionHandlerFunc(func(params transactions.CreateTransactionParams) middleware.Responder {
			return middleware.NotImplemented("operation transactions.CreateTransaction has not yet been implemented")
		})
	}
	if api.UsersDeletePaymentInfoHandler == nil {
		api.UsersDeletePaymentInfoHandler = users.DeletePaymentInfoHandlerFunc(func(params users.DeletePaymentInfoParams) middleware.Responder {
			return middleware.NotImplemented("operation users.DeletePaymentInfo has not yet been implemented")
		})
	}
	if api.NewsGetCategoriesHandler == nil {
		api.NewsGetCategoriesHandler = news.GetCategoriesHandlerFunc(func(params news.GetCategoriesParams) middleware.Responder {
			return middleware.NotImplemented("operation news.GetCategories has not yet been implemented")
		})
	}
	if api.StocksGetCurrenciesHandler == nil {
		api.StocksGetCurrenciesHandler = stocks.GetCurrenciesHandlerFunc(func(params stocks.GetCurrenciesParams) middleware.Responder {
			return middleware.NotImplemented("operation stocks.GetCurrencies has not yet been implemented")
		})
	}
	if api.StocksGetCurrentStocksHandler == nil {
		api.StocksGetCurrentStocksHandler = stocks.GetCurrentStocksHandlerFunc(func(params stocks.GetCurrentStocksParams) middleware.Responder {
			return middleware.NotImplemented("operation stocks.GetCurrentStocks has not yet been implemented")
		})
	}
	if api.NewsGetNewsHandler == nil {
		api.NewsGetNewsHandler = news.GetNewsHandlerFunc(func(params news.GetNewsParams) middleware.Responder {
			return middleware.NotImplemented("operation news.GetNews has not yet been implemented")
		})
	}
	if api.NotificationsGetNotificationsHandler == nil {
		api.NotificationsGetNotificationsHandler = notifications.GetNotificationsHandlerFunc(func(params notifications.GetNotificationsParams) middleware.Responder {
			return middleware.NotImplemented("operation notifications.GetNotifications has not yet been implemented")
		})
	}
	if api.DefaultOperationsGetShitHandler == nil {
		api.DefaultOperationsGetShitHandler = default_operations.GetShitHandlerFunc(func(params default_operations.GetShitParams) middleware.Responder {
			return middleware.NotImplemented("operation default_operations.GetShit has not yet been implemented")
		})
	}
	if api.StocksGetSingleStockHandler == nil {
		api.StocksGetSingleStockHandler = stocks.GetSingleStockHandlerFunc(func(params stocks.GetSingleStockParams) middleware.Responder {
			return middleware.NotImplemented("operation stocks.GetSingleStock has not yet been implemented")
		})
	}
	if api.StocksGetStocksForGhaphHandler == nil {
		api.StocksGetStocksForGhaphHandler = stocks.GetStocksForGhaphHandlerFunc(func(params stocks.GetStocksForGhaphParams) middleware.Responder {
			return middleware.NotImplemented("operation stocks.GetStocksForGhaph has not yet been implemented")
		})
	}
	if api.UsersGetUserHandler == nil {
		api.UsersGetUserHandler = users.GetUserHandlerFunc(func(params users.GetUserParams) middleware.Responder {
			return middleware.NotImplemented("operation users.GetUser has not yet been implemented")
		})
	}
	if api.TransactionsGetUserPortfolioHandler == nil {
		api.TransactionsGetUserPortfolioHandler = transactions.GetUserPortfolioHandlerFunc(func(params transactions.GetUserPortfolioParams) middleware.Responder {
			return middleware.NotImplemented("operation transactions.GetUserPortfolio has not yet been implemented")
		})
	}
	if api.TransactionsGetUserTransactionsHandler == nil {
		api.TransactionsGetUserTransactionsHandler = transactions.GetUserTransactionsHandlerFunc(func(params transactions.GetUserTransactionsParams) middleware.Responder {
			return middleware.NotImplemented("operation transactions.GetUserTransactions has not yet been implemented")
		})
	}
	if api.AdminGetUsersWithParamsHandler == nil {
		api.AdminGetUsersWithParamsHandler = admin.GetUsersWithParamsHandlerFunc(func(params admin.GetUsersWithParamsParams) middleware.Responder {
			return middleware.NotImplemented("operation admin.GetUsersWithParams has not yet been implemented")
		})
	}
	if api.DefaultOperationsHealthCheckHandler == nil {
		api.DefaultOperationsHealthCheckHandler = default_operations.HealthCheckHandlerFunc(func(params default_operations.HealthCheckParams) middleware.Responder {
			return middleware.NotImplemented("operation default_operations.HealthCheck has not yet been implemented")
		})
	}
	if api.AuthLoginUserHandler == nil {
		api.AuthLoginUserHandler = auth.LoginUserHandlerFunc(func(params auth.LoginUserParams) middleware.Responder {
			return middleware.NotImplemented("operation auth.LoginUser has not yet been implemented")
		})
	}
	if api.UsersRegisterUserHandler == nil {
		api.UsersRegisterUserHandler = users.RegisterUserHandlerFunc(func(params users.RegisterUserParams) middleware.Responder {
			return middleware.NotImplemented("operation users.RegisterUser has not yet been implemented")
		})
	}
	if api.NotificationsSetNotificationHandler == nil {
		api.NotificationsSetNotificationHandler = notifications.SetNotificationHandlerFunc(func(params notifications.SetNotificationParams) middleware.Responder {
			return middleware.NotImplemented("operation notifications.SetNotification has not yet been implemented")
		})
	}
	if api.AdminSwitchUserStateHandler == nil {
		api.AdminSwitchUserStateHandler = admin.SwitchUserStateHandlerFunc(func(params admin.SwitchUserStateParams) middleware.Responder {
			return middleware.NotImplemented("operation admin.SwitchUserState has not yet been implemented")
		})
	}
	if api.UsersUpdateUserInfoHandler == nil {
		api.UsersUpdateUserInfoHandler = users.UpdateUserInfoHandlerFunc(func(params users.UpdateUserInfoParams) middleware.Responder {
			return middleware.NotImplemented("operation users.UpdateUserInfo has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix".
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation.
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics.
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
