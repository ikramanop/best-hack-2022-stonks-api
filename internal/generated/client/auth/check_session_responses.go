// Code generated by go-swagger; DO NOT EDIT.

package auth

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
)

// CheckSessionReader is a Reader for the CheckSession structure.
type CheckSessionReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *CheckSessionReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewCheckSessionOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 400:
		result := NewCheckSessionBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewCheckSessionOK creates a CheckSessionOK with default headers values
func NewCheckSessionOK() *CheckSessionOK {
	return &CheckSessionOK{}
}

/* CheckSessionOK describes a response with status code 200, with default header values.

Success
*/
type CheckSessionOK struct {
	Payload *models.TokenResponse
}

func (o *CheckSessionOK) Error() string {
	return fmt.Sprintf("[POST /auth/checkSession][%d] checkSessionOK  %+v", 200, o.Payload)
}
func (o *CheckSessionOK) GetPayload() *models.TokenResponse {
	return o.Payload
}

func (o *CheckSessionOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.TokenResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewCheckSessionBadRequest creates a CheckSessionBadRequest with default headers values
func NewCheckSessionBadRequest() *CheckSessionBadRequest {
	return &CheckSessionBadRequest{}
}

/* CheckSessionBadRequest describes a response with status code 400, with default header values.

Failure
*/
type CheckSessionBadRequest struct {
	Payload *models.BaseAPIResponse
}

func (o *CheckSessionBadRequest) Error() string {
	return fmt.Sprintf("[POST /auth/checkSession][%d] checkSessionBadRequest  %+v", 400, o.Payload)
}
func (o *CheckSessionBadRequest) GetPayload() *models.BaseAPIResponse {
	return o.Payload
}

func (o *CheckSessionBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.BaseAPIResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
