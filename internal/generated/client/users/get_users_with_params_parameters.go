// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"
	"net/http"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	cr "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
)

// NewGetUsersWithParamsParams creates a new GetUsersWithParamsParams object,
// with the default timeout for this client.
//
// Default values are not hydrated, since defaults are normally applied by the API server side.
//
// To enforce default values in parameter, use SetDefaults or WithDefaults.
func NewGetUsersWithParamsParams() *GetUsersWithParamsParams {
	return &GetUsersWithParamsParams{
		timeout: cr.DefaultTimeout,
	}
}

// NewGetUsersWithParamsParamsWithTimeout creates a new GetUsersWithParamsParams object
// with the ability to set a timeout on a request.
func NewGetUsersWithParamsParamsWithTimeout(timeout time.Duration) *GetUsersWithParamsParams {
	return &GetUsersWithParamsParams{
		timeout: timeout,
	}
}

// NewGetUsersWithParamsParamsWithContext creates a new GetUsersWithParamsParams object
// with the ability to set a context for a request.
func NewGetUsersWithParamsParamsWithContext(ctx context.Context) *GetUsersWithParamsParams {
	return &GetUsersWithParamsParams{
		Context: ctx,
	}
}

// NewGetUsersWithParamsParamsWithHTTPClient creates a new GetUsersWithParamsParams object
// with the ability to set a custom HTTPClient for a request.
func NewGetUsersWithParamsParamsWithHTTPClient(client *http.Client) *GetUsersWithParamsParams {
	return &GetUsersWithParamsParams{
		HTTPClient: client,
	}
}

/* GetUsersWithParamsParams contains all the parameters to send to the API endpoint
   for the get users with params operation.

   Typically these are written to a http.Request.
*/
type GetUsersWithParamsParams struct {

	// Body.
	Body *models.GetUsersWithParamsRequest

	timeout    time.Duration
	Context    context.Context
	HTTPClient *http.Client
}

// WithDefaults hydrates default values in the get users with params params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetUsersWithParamsParams) WithDefaults() *GetUsersWithParamsParams {
	o.SetDefaults()
	return o
}

// SetDefaults hydrates default values in the get users with params params (not the query body).
//
// All values with no default are reset to their zero value.
func (o *GetUsersWithParamsParams) SetDefaults() {
	// no default values defined for this parameter
}

// WithTimeout adds the timeout to the get users with params params
func (o *GetUsersWithParamsParams) WithTimeout(timeout time.Duration) *GetUsersWithParamsParams {
	o.SetTimeout(timeout)
	return o
}

// SetTimeout adds the timeout to the get users with params params
func (o *GetUsersWithParamsParams) SetTimeout(timeout time.Duration) {
	o.timeout = timeout
}

// WithContext adds the context to the get users with params params
func (o *GetUsersWithParamsParams) WithContext(ctx context.Context) *GetUsersWithParamsParams {
	o.SetContext(ctx)
	return o
}

// SetContext adds the context to the get users with params params
func (o *GetUsersWithParamsParams) SetContext(ctx context.Context) {
	o.Context = ctx
}

// WithHTTPClient adds the HTTPClient to the get users with params params
func (o *GetUsersWithParamsParams) WithHTTPClient(client *http.Client) *GetUsersWithParamsParams {
	o.SetHTTPClient(client)
	return o
}

// SetHTTPClient adds the HTTPClient to the get users with params params
func (o *GetUsersWithParamsParams) SetHTTPClient(client *http.Client) {
	o.HTTPClient = client
}

// WithBody adds the body to the get users with params params
func (o *GetUsersWithParamsParams) WithBody(body *models.GetUsersWithParamsRequest) *GetUsersWithParamsParams {
	o.SetBody(body)
	return o
}

// SetBody adds the body to the get users with params params
func (o *GetUsersWithParamsParams) SetBody(body *models.GetUsersWithParamsRequest) {
	o.Body = body
}

// WriteToRequest writes these params to a swagger request
func (o *GetUsersWithParamsParams) WriteToRequest(r runtime.ClientRequest, reg strfmt.Registry) error {

	if err := r.SetTimeout(o.timeout); err != nil {
		return err
	}
	var res []error
	if o.Body != nil {
		if err := r.SetBodyParam(o.Body); err != nil {
			return err
		}
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
