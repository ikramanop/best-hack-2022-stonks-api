// Code generated by go-swagger; DO NOT EDIT.

package users

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"
	"io"

	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/models"
)

// ChangeUserRoleReader is a Reader for the ChangeUserRole structure.
type ChangeUserRoleReader struct {
	formats strfmt.Registry
}

// ReadResponse reads a server response into the received o.
func (o *ChangeUserRoleReader) ReadResponse(response runtime.ClientResponse, consumer runtime.Consumer) (interface{}, error) {
	switch response.Code() {
	case 200:
		result := NewChangeUserRoleOK()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return result, nil
	case 400:
		result := NewChangeUserRoleBadRequest()
		if err := result.readResponse(response, consumer, o.formats); err != nil {
			return nil, err
		}
		return nil, result
	default:
		return nil, runtime.NewAPIError("response status code does not match any response statuses defined for this endpoint in the swagger spec", response, response.Code())
	}
}

// NewChangeUserRoleOK creates a ChangeUserRoleOK with default headers values
func NewChangeUserRoleOK() *ChangeUserRoleOK {
	return &ChangeUserRoleOK{}
}

/* ChangeUserRoleOK describes a response with status code 200, with default header values.

Success
*/
type ChangeUserRoleOK struct {
	Payload *models.BaseAPIResponse
}

func (o *ChangeUserRoleOK) Error() string {
	return fmt.Sprintf("[PATCH /users/{uuid}/changeRole][%d] changeUserRoleOK  %+v", 200, o.Payload)
}
func (o *ChangeUserRoleOK) GetPayload() *models.BaseAPIResponse {
	return o.Payload
}

func (o *ChangeUserRoleOK) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.BaseAPIResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}

// NewChangeUserRoleBadRequest creates a ChangeUserRoleBadRequest with default headers values
func NewChangeUserRoleBadRequest() *ChangeUserRoleBadRequest {
	return &ChangeUserRoleBadRequest{}
}

/* ChangeUserRoleBadRequest describes a response with status code 400, with default header values.

Failure
*/
type ChangeUserRoleBadRequest struct {
	Payload *models.BaseAPIResponse
}

func (o *ChangeUserRoleBadRequest) Error() string {
	return fmt.Sprintf("[PATCH /users/{uuid}/changeRole][%d] changeUserRoleBadRequest  %+v", 400, o.Payload)
}
func (o *ChangeUserRoleBadRequest) GetPayload() *models.BaseAPIResponse {
	return o.Payload
}

func (o *ChangeUserRoleBadRequest) readResponse(response runtime.ClientResponse, consumer runtime.Consumer, formats strfmt.Registry) error {

	o.Payload = new(models.BaseAPIResponse)

	// response payload
	if err := consumer.Consume(response.Body(), o.Payload); err != nil && err != io.EOF {
		return err
	}

	return nil
}
