package model

type Currency struct {
	ID uint64 `db:"id"`

	Tag string `db:"tag"`

	Name string `db:"name"`
}
