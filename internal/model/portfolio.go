package model

type Portfolio struct {
	Name string

	Amount int64

	TotalPrice float64

	ChangePlain float64

	ChangePercent float64
}
