package model

import "time"

type Price struct {
	ID uint64 `db:"id"`

	SourceCurrency string `db:"source_currency"`

	TargetCurrency string `db:"target_currency"`

	Price float64 `db:"price"`

	Timestamp time.Time `db:"timestamp"`
}
