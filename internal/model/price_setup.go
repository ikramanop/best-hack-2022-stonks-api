package model

type PriceSetup struct {
	ID uint64 `db:"id"`

	TargetCurrency string `db:"target_currency"`

	SourceCurrency string `db:"source_currency"`

	BuyDelta float64 `db:"buy_delta"`

	SellDelta float64 `db:"sell_delta"`
}
