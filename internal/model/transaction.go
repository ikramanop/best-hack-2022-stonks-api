package model

import "time"

type Transaction struct {
	ID uint64 `db:"id"`

	UserUUID string `db:"user_uuid"`

	Name string `db:"name"`

	Gain int64 `db:"gain"`

	Price float64 `db:"price"`

	CreatedAt time.Time `db:"created_at"`
}
