package model

import "time"

type Notification struct {
	ID uint64 `db:"id"`

	UserUUID string `db:"user_uuid"`

	Payload string `db:"payload"`

	Seen bool `db:"seen"`

	CreatedAt time.Time `db:"created_at"`
}
