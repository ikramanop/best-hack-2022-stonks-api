package model

import "time"

type Schedule struct {
	ID uint64 `db:"id"`

	UserUUID string `db:"user_uuid"`

	PercentChange float64 `db:"percent_change"`

	SourceCurrency string `db:"source_currency"`

	TargetCurrency string `db:"target_currency"`

	CreatedAt time.Time `db:"created_at"`
}
