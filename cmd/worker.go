package cmd

import (
	"context"
	"github.com/asvvvad/exchange"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/prices"
	"log"
	"os"
	"os/signal"

	"github.com/robfig/cron/v3"
	"github.com/spf13/cobra"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/currencies"
)

var runWorkerCMD = &cobra.Command{
	Use:   "worker",
	Short: "Run worker",
	Long:  "Run worker",
	RunE: func(cmd *cobra.Command, args []string) error {
		return runWorker()
	},
}

func runWorker() error {
	c := cron.New()
	_, err := c.AddFunc(os.Getenv("CURRENCIES_UPDATE_CRON_INTERVAL"), updateQuotes)
	if err != nil {
		return err
	}
	go c.Start()

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, os.Kill)
	<-sig

	return nil
}

func updateQuotes() {
	rxc, err := currencies.New()
	if err != nil {
		log.Println(err)
		return
	}
	defer rxc.Close(context.Background())

	currenciesRaw, ok, err := rxc.Get()
	if err != nil {
		log.Println(err)
		return
	}
	if !ok {
		log.Println("error getting supported currencies")
		return
	}

	rxp, err := prices.New()
	if err != nil {
		log.Println(err)
		return
	}
	defer rxp.Close(context.Background())

	for _, sourceCurrency := range currenciesRaw {
		for _, targetCurrency := range currenciesRaw {
			if sourceCurrency.Tag == targetCurrency.Tag {
				continue
			}

			ex := exchange.New(sourceCurrency.Tag)
			rateRaw, err := ex.ConvertTo(targetCurrency.Tag, 1)
			if err != nil {
				if err != nil {
					log.Println(err)
					continue
				}
			}

			rate, _ := rateRaw.Float64()

			ok, err := rxp.Add(sourceCurrency.Tag, targetCurrency.Tag, rate)
			if err != nil {
				log.Println(err)
				continue
			}
			if !ok {
				log.Printf("cant update %s to %s prices", sourceCurrency.Tag, targetCurrency.Tag)
			}
		}
	}

	log.Println("quotes updated successfully")
}
