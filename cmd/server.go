package cmd

import (
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/admin"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/news"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/notifications"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/support"
	"log"
	"os"
	"strconv"

	"github.com/go-openapi/loads"
	"github.com/spf13/cobra"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/default_operations"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/stocks"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/transactions"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/generated/restapi/operations/users"
	admin2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/handlers/admin"
	auth2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/handlers/auth"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/handlers/default"
	news2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/handlers/news"
	notifications2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/handlers/notifications"
	stocks2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/handlers/stocks"
	support2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/handlers/support"
	transactions2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/handlers/transactions"
	users2 "gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/handlers/users"
)

var runServerCMD = &cobra.Command{
	Use:   "server",
	Short: "Run server",
	Long:  "Run server",
	RunE: func(cmd *cobra.Command, args []string) error {
		return runServer()
	},
}

func runServer() error {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		return err
	}

	api := operations.NewAPIAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer func() {
		if err := server.Shutdown(); err != nil {
			log.Fatalln(err)
		}
	}()

	port, err := strconv.Atoi(os.Getenv("STONKS_API_PORT"))
	if err != nil {
		return err
	}

	server.Port = port

	api.DefaultOperationsHealthCheckHandler = default_operations.HealthCheckHandlerFunc(_default.HealthCheckHandler)
	api.DefaultOperationsGetShitHandler = default_operations.GetShitHandlerFunc(_default.GetShitHandler)

	api.StocksGetSingleStockHandler = stocks.GetSingleStockHandlerFunc(stocks2.GetSingleStockHandler)
	api.StocksGetCurrentStocksHandler = stocks.GetCurrentStocksHandlerFunc(stocks2.GetCurrentStocksHandler)
	api.StocksGetStocksForGhaphHandler = stocks.GetStocksForGhaphHandlerFunc(stocks2.GetStocksForGraphHandler)
	api.StocksGetCurrenciesHandler = stocks.GetCurrenciesHandlerFunc(stocks2.GetCurrenciesHandler)

	api.UsersDeletePaymentInfoHandler = users.DeletePaymentInfoHandlerFunc(users2.DeletePaymentInfoHandler)
	api.UsersAddPaymentInfoHandler = users.AddPaymentInfoHandlerFunc(users2.AddPaymentInfoHandler)
	api.UsersUpdateUserInfoHandler = users.UpdateUserInfoHandlerFunc(users2.UpdateUserInfoHandler)
	api.UsersGetUserHandler = users.GetUserHandlerFunc(users2.GetUserHandler)
	api.UsersRegisterUserHandler = users.RegisterUserHandlerFunc(users2.RegisterUserHandler)
	api.UsersChangeBalanceHandler = users.ChangeBalanceHandlerFunc(users2.ChangeBalanceHandler)

	api.AuthLoginUserHandler = auth.LoginUserHandlerFunc(auth2.LoginUserHandler)

	api.TransactionsGetUserTransactionsHandler = transactions.GetUserTransactionsHandlerFunc(transactions2.GetUserTransactionsHandler)
	api.TransactionsGetUserPortfolioHandler = transactions.GetUserPortfolioHandlerFunc(transactions2.GetUserPortfolioHandler)
	api.TransactionsCreateTransactionHandler = transactions.CreateTransactionHandlerFunc(transactions2.CreateTransactionHandler)

	api.NewsGetCategoriesHandler = news.GetCategoriesHandlerFunc(news2.GetCategoriesHandler)
	api.NewsGetNewsHandler = news.GetNewsHandlerFunc(news2.GetNewsHandler)

	api.NotificationsSetNotificationHandler = notifications.SetNotificationHandlerFunc(notifications2.SetNotificationHandler)
	api.NotificationsGetNotificationsHandler = notifications.GetNotificationsHandlerFunc(notifications2.GetNotificationsHandler)

	api.SupportCreateSupportRequestHandler = support.CreateSupportRequestHandlerFunc(support2.CreateSupportRequestHandler)

	api.AdminGetUsersWithParamsHandler = admin.GetUsersWithParamsHandlerFunc(admin2.GetUsersWithParams)
	api.AdminChangeUserRoleHandler = admin.ChangeUserRoleHandlerFunc(admin2.ChangeUserRole)
	api.AdminSwitchUserStateHandler = admin.SwitchUserStateHandlerFunc(admin2.SwitchUserState)

	if err := server.Serve(); err != nil {
		return err
	}

	return nil
}
