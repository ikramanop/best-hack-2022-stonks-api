package cmd

import (
	"context"
	"fmt"
	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/prices"
	"log"
	"os"
	"os/signal"

	"github.com/robfig/cron/v3"
	"github.com/spf13/cobra"

	"gitlab.com/ikramanop/best-hack-2022-stonks-api/internal/repository/notifications"
)

var runNotificationsCMD = &cobra.Command{
	Use:   "notifications",
	Short: "Run notifications",
	Long:  "Run notifications",
	RunE: func(cmd *cobra.Command, args []string) error {
		return runNotifications()
	},
}

func runNotifications() error {
	c := cron.New()
	_, err := c.AddFunc(os.Getenv("NOTIFICATIONS_UPDATE_CRON_INTERVAL"), scheduleNotifications)
	if err != nil {
		return err
	}
	go c.Start()

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, os.Kill)
	<-sig

	return nil
}

func scheduleNotifications() {
	rxc, err := notifications.New()
	if err != nil {
		log.Println(err)
		return
	}
	defer rxc.Close(context.Background())

	schedules, ok, err := rxc.GetSchedules()
	if err != nil {
		log.Println(err)
		return
	}
	if !ok {
		log.Println("no schedules found")
		return
	}

	rxp, err := prices.New()
	if err != nil {
		log.Println(err)
		return
	}
	defer rxp.Close(context.Background())

	for _, schedule := range schedules {
		current, ok, err := rxp.Get(schedule.TargetCurrency, schedule.SourceCurrency)
		if err != nil {
			log.Println(err)
			continue
		}
		if !ok {
			log.Println("no prices found")
			continue
		}

		previous, ok, err := rxp.GetWithDate(schedule.TargetCurrency, schedule.SourceCurrency, schedule.CreatedAt)
		if err != nil {
			log.Println(err)
			continue
		}
		if !ok {
			log.Println("no prices found")
			continue
		}

		percent := (current.Price - previous.Price) / previous.Price * 100

		pass := false
		if schedule.PercentChange >= 0 {
			if percent >= schedule.PercentChange {
				pass = true
			}
		} else {
			if percent <= schedule.PercentChange {
				pass = true
			}
		}

		if pass {
			payload := fmt.Sprintf("Курс валют %s/%s изменился на %d",
				schedule.SourceCurrency, schedule.TargetCurrency, schedule.PercentChange)
			ok, err := rxc.AddNotification(schedule.UserUUID, payload)
			if err != nil {
				log.Println(err)
				continue
			}
			if !ok {
				log.Println("no notifications set")
				continue
			}
			ok, err = rxc.DeleteSchedule(schedule.ID)
			if err != nil {
				log.Println(err)
				continue
			}
			if !ok {
				log.Println("no schedules deleted")
				continue
			}
		}
	}

	log.Println("notifications updated successfully")
}
