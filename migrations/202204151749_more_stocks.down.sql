begin;

delete
from stocks
where tag = 'AMBA';

delete
from stocks
where tag = 'TWTR';

delete
from stocks
where tag = 'UA';

delete
from stocks
where tag = 'GRPN';

delete
from stocks
where tag = 'YELP';

delete
from stocks
where tag = 'NKE';

delete
from stocks
where tag = 'CMG';

delete
from stocks
where tag = 'RL';

delete
from stocks
where tag = 'LULU';

commit;