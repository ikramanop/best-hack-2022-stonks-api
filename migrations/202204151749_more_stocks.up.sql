begin;

insert into stocks (tag, name)
values ('AMBA', 'Ambarella, Inc.');

insert into stocks (tag, name)
values ('TWTR', 'Twitter, Inc.');

insert into stocks (tag, name)
values ('UA', 'Under Armour, Inc.');

insert into stocks (tag, name)
values ('GRPN', 'Groupon, Inc.');

insert into stocks (tag, name)
values ('YELP', 'Yelp, Inc.');

insert into stocks (tag, name)
values ('NKE', 'NIKE, Inc.');

insert into stocks (tag, name)
values ('CMG', 'Chipotle Mexican Grill, Inc.');

insert into stocks (tag, name)
values ('RL', 'Ralph Lauren Corporation');

insert into stocks (tag, name)
values ('LULU', 'Lululemon Athletica, Inc.');

commit;