begin;

delete
from currencies
where tag = 'RUB';

delete
from currencies
where tag = 'USD';

delete
from currencies
where tag = 'EUR';

delete
from currencies
where tag = 'CNY';

delete
from currencies
where tag = 'JPY';

delete
from currencies
where tag = 'AMD';

commit;