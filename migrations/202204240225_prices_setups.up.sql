begin;

create table if not exists price_setups
(
    id              serial primary key not null,
    target_currency text               not null,
    source_currency text               not null,
    buy_delta       float              not null default -0.05,
    sell_delta      float              not null default 0.05
);

insert into price_setups (target_currency, source_currency)
values ('RUB', 'USD');

insert into price_setups (target_currency, source_currency)
values ('RUB', 'EUR');

insert into price_setups (target_currency, source_currency)
values ('RUB', 'CNY');

insert into price_setups (target_currency, source_currency)
values ('RUB', 'JPY');

insert into price_setups (target_currency, source_currency)
values ('RUB', 'AMD');

insert into price_setups (target_currency, source_currency)
values ('USD', 'RUB');

insert into price_setups (target_currency, source_currency)
values ('USD', 'EUR');

insert into price_setups (target_currency, source_currency)
values ('USD', 'CNY');

insert into price_setups (target_currency, source_currency)
values ('USD', 'JPY');

insert into price_setups (target_currency, source_currency)
values ('USD', 'AMD');

insert into price_setups (target_currency, source_currency)
values ('EUR', 'RUB');

insert into price_setups (target_currency, source_currency)
values ('EUR', 'USD');

insert into price_setups (target_currency, source_currency)
values ('EUR', 'CNY');

insert into price_setups (target_currency, source_currency)
values ('EUR', 'JPY');

insert into price_setups (target_currency, source_currency)
values ('EUR', 'AMD');

insert into price_setups (target_currency, source_currency)
values ('CNY', 'RUB');

insert into price_setups (target_currency, source_currency)
values ('CNY', 'USD');

insert into price_setups (target_currency, source_currency)
values ('CNY', 'EUR');

insert into price_setups (target_currency, source_currency)
values ('CNY', 'JPY');

insert into price_setups (target_currency, source_currency)
values ('CNY', 'AMD');

insert into price_setups (target_currency, source_currency)
values ('JPY', 'RUB');

insert into price_setups (target_currency, source_currency)
values ('JPY', 'USD');

insert into price_setups (target_currency, source_currency)
values ('JPY', 'EUR');

insert into price_setups (target_currency, source_currency)
values ('JPY', 'CNY');

insert into price_setups (target_currency, source_currency)
values ('JPY', 'AMD');

insert into price_setups (target_currency, source_currency)
values ('AMD', 'RUB');

insert into price_setups (target_currency, source_currency)
values ('AMD', 'USD');

insert into price_setups (target_currency, source_currency)
values ('AMD', 'EUR');

insert into price_setups (target_currency, source_currency)
values ('AMD', 'CNY');

insert into price_setups (target_currency, source_currency)
values ('AMD', 'JPY');

commit;