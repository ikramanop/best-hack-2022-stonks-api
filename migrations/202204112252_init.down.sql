begin;

drop table if exists log;
drop table if exists transactions;
drop table if exists prices;
drop table if exists currencies;

commit;