begin;

insert into currencies (tag, name)
values ('RUB', 'Russian Rouble');

insert into currencies (tag, name)
values ('USD', 'US Dollar');

insert into currencies (tag, name)
values ('EUR', 'Euro');

insert into currencies (tag, name)
values ('CNY', 'Yuan Renminbi');

insert into currencies (tag, name)
values ('JPY', 'Yen');

insert into currencies (tag, name)
values ('AMD', 'Armenian Dram');

commit;