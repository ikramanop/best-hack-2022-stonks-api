begin;

create table if not exists currencies
(
    id   serial primary key not null,
    tag  text               not null,
    name text               not null
);

create table if not exists prices
(
    id              serial primary key not null,
    source_currency text               not null,
    target_currency text               not null,
    price           float              not null,
    timestamp       timestamptz        not null default now()
);

create table if not exists transactions
(
    id         serial primary key not null,
    user_uuid  uuid               not null,
    name       text               not null,
    gain       integer            not null,
    price      float              not null,
    created_at timestamptz        not null default now()
);

create table if not exists log
(
    id         serial primary key not null,
    user_uuid  uuid               not null,
    payload    text               not null,
    created_at timestamptz        not null default now()
);

commit;