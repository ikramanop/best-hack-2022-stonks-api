begin;

create table if not exists schedule
(
    id              serial primary key not null,
    user_uuid       text               not null,
    percent_change  float              not null,
    target_currency text               not null,
    source_currency text               not null,
    created_at      timestamptz        not null default now()
);

create table if not exists notifications
(
    id         serial primary key not null,
    user_uuid  text               not null,
    payload    text               not null,
    seen       bool               not null default false,
    created_at timestamptz        not null default now()
);

commit;